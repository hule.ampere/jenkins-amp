#!/bin/sh

CONFIG_FILE="$PWD/CONFIG"

if [ ! -f $CONFIG_FILE ]; then
	echo "NOT FOUND THE CONFIG FILE"
	exit -1
fi

source $CONFIG_FILE

if [ ! -d "$PWD/$PID_DIR" ]; then
	echo "NOT FOUND PID DIRECTORY"
	exit -1
fi

for file in $PWD/$PID_DIR/*; do
	if [[ -f $file ]]; then
		PID="$(cat $file)"
		if [ -n "$PID" -a -e /proc/$PID ]; then
			echo "kill -2 $PID"
			kill -2 $PID
		fi
	fi
done

pgrep tail | xargs kill