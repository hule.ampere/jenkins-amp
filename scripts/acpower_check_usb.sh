#!/bin/bash

CONFIG_FILE="$PWD/CONFIG"
COUNTER=5
if [ ! -f $CONFIG_FILE ]; then
│ echo "NOT FOUND THE CONFIG FILE"
│ exit -1
fi

#while true; do
#│ if [ $COUNTER -eq 0 ]; then
#│ │ break
#│ fi
#│ if [ -z $BMC_IPADDRESS ]; then
#│ │ source $CONFIG_FILE
#│ │ sleep 5s
#│ │ COUNTER=$[$COUNTER-1]
#│ │ continue
#│ fi

#│ break
#done

cpuip="10.38.65.23"
pass="root"
user="root"

sshpass -p "${pass}" ssh -t ${user}@${cpuip} "which lsusb"
if [ $? -ne 0 ]; then
	yum install usbutils -y
fi

echo "Run AC Cycle Test on CPU IP $cpuip"
TEST_TIMES=10
AC_RUN=1
while [ $TEST_TIMES -gt 0 ]; do
	# Reset the system
	echo "========== Raptor AC cycle test $AC_RUN... =========="
	echo " - Run A/C ..."
  ./scripts/acpower_on_off 10.38.64.16 apc apc-c 1 off
  sleep 30
  ./scripts/acpower_on_off 10.38.64.16 apc apc-c 1 on

	# Wait 5 minutes for the system to complete boot
	echo " - Sleep 5 minutes to wait for Linux boot complete ..."
  sleep 250

	# Check if the system boots success by running a IPMI command
#	echo "sshpass -p "${pass}" ssh -t ${user}@${cpuip}"

	# Check Virtual CD-ROM
#	sshpass -p "${pass}" ssh -t ${user}@${cpuip} "ls /dev/sr0"
#	if [ $? -ne 0 ]; then
#		echo "Error: Missing CD-ROM"
#	fi

#	sshpass -p "${pass}" ssh -t ${user}@${cpuip} "lsscsi | grep HDisk0"
#	if [ $? -ne 0 ]; then
#		echo "Error: Missing Virtual HDisk"
#	fi

	sshpass -p "${pass}" ssh -t ${user}@${cpuip} "/root/scripts/check_usb.sh"

#	sshpass -p "${pass}" ssh -t ${user}@${cpuip} "lsusb | grep \"Keyboard and Mouse\""
#	if [ $? -ne 0 ]; then
#		echo "Error: Missing Virtual Keyboard and Mouse"
#	fi
  AC_RUN=$[$AC_RUN+1]
#  sleep 120
done
./scripts/killall.sh

