#!/usr/bin/env expect

set timeout -1

set ip [lindex $argv 0]
set user [lindex $argv 1]
set password [lindex $argv 2]
set deactivate "ipmitool -I lanplus -U $user -P $password -H $ip sol deactivate > /dev/null"
set activate "ipmitool -I lanplus -U $user -P $password -H $ip sol activate nokeepalive"

spawn bash
send "$deactivate\r"
sleep 10
send "$activate\r"
sleep 5

expect "help]" {
	send "\r"
	exp_continue
}

interact