#!/bin/bash
#./acpower_on_off 10.38.64.16 apc apc-c 5 on/off
pass="root"
user="root"
cpuip="10.38.65.23"
COUNTER=0
while [  $COUNTER -lt 1000 ]; do
  echo Reset times: $COUNTER
  let COUNTER=COUNTER+1
  ./acpower_on_off 10.38.64.16 apc apc-c 1 off
  sleep 30s
  ./acpower_on_off 10.38.64.16 apc apc-c 1 on
  sleep 4m
#  sshpass -p "${pass}" ssh -t ${user}@${cpuip} "/root/scripts/check_usb.sh"
  sshpass -p "${pass}" ssh -t ${user}@${cpuip} "lsusb"
  sshpass -p "${pass}" ssh -t ${user}@${cpuip} "lsscsi"
 
# Check Virtual CD-ROM
  sshpass -p "${pass}" ssh -t ${user}@${cpuip} "ls /dev/sr0"
  if [ $? -ne 0 ]; then
    echo "Error: Missing CD-ROM"
  fi

  sshpass -p "${pass}" ssh -t ${user}@${cpuip} "lsscsi | grep HDisk0"
  if [ $? -ne 0 ]; then
    echo "Error: Missing Virtual HDisk"
  fi

  sshpass -p "${pass}" ssh -t ${user}@${cpuip} "lsscsi | grep CDROM0"
  if [ $? -ne 0 ]; then
    echo "Error: Missing Virtual HDisk"
  fi
done
