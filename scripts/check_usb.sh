#!/bin/bash

which lsusb
if [ $? -ne 0 ]; then
	yum install usbutils -y
fi
# Check Virtual CD-ROM
ls /dev/sr0
if [ $? -ne 0 ]; then
	echo "Error: Missing CD-ROM"
fi

lsscsi | grep HDisk0
if [ $? -ne 0 ]; then
	echo "Error: Missing Virtual HDisk"
fi

lsscsi | grep CDROM0
if [ $? -ne 0 ]; then
	echo "Error: Missing Virtual HDisk"
fi
