#!/bin/bash

#--------------------------------------------------------------------
# fwupdate.sh is intended to be used to upgrade SCP and BIOS firmware
# using fwupdate tool. It is expected to have fwupdate tool installed
# correctly.
# Limitation: only support CentOS, currently
#--------------------------------------------------------------------

function fwupdate(){
	addr=$1
	capfile=$2
	user=$3
	pass=$4
	tmpdir="/tmp/fwupdate"
	capname=$(basename $capfile)

	# Check if the CPU is pingable
	ping -q -c 1 -W 1 ${addr} > /dev/null
	if [ $? != 0 ]; then
		echo "Error: $addr is unreachable. Please check"
		exit
	fi

	# Check if the CPU can be logged via ssh
	sshpass -p "${pass}" ssh -t ${user}@${addr} "ls > /dev/null"
	if [ $? != 0 ]; then
		echo "Error: can not execute command on $ipaddress via ssh. Please check"
		exit
	fi

	# Copy *.cap image to the server
	sshpass -p "${pass}" ssh -t ${user}@${addr} "mkdir -p $tmpdir"
	sshpass -p "${pass}" scp ${capfile} ${user}@${addr}:$tmpdir

	# Check fwupdate
	sshpass -p "${pass}" ssh -t ${user}@${addr} "fwupdate -l"
	# Run fwupdate
	sshpass -p "${pass}" ssh -t ${user}@${addr} "mv /boot/efi/EFI/centos/shim*.efi $tmpdir"
	#sshpass -p "${pass}" ssh -t ${user}@${addr} "mv /boot/efi/EFI/BOOT/fallback.efi $tmpdir"
	#sshpass -p "${pass}" ssh -t ${user}@${addr} "fwupdate -a {62af20c0-7016-424a-9bf8-9ccc86584090} $tmpdir/${capname}"
	#sshpass -p "${pass}" ssh -t ${user}@${addr} "fwupdate -F -d -a {62af20c0-7016-424a-9bf8-9ccc86584090} $tmpdir/${capname}"
	sshpass -p "${pass}" ssh -t ${user}@${addr} "fwupdate -e -F -d -a {62af20c0-7016-424a-9bf8-9ccc86584090} $tmpdir/${capname}"
	sshpass -p "${pass}" ssh -t ${user}@${addr} "fwupdate -L"
	sshpass -p "${pass}" ssh -t ${user}@${addr} "fwupdate -i"
	sshpass -p "${pass}" ssh -t ${user}@${addr} "mv $tmpdir/shim*.efi /boot/efi/EFI/centos"
	#sshpass -p "${pass}" ssh -t ${user}@${addr} "mv $tmpdir/fallback.efi /boot/efi/EFI/BOOT"
	#sshpass -p "${pass}" ssh -t ${user}@${addr} "rm -rf $tmpdir"

	# Reboot to take affect
	sshpass -p "${pass}" ssh -t ${user}@${addr} "reboot"
}

cpuip=$1
capfile=$2
usr=$3
pass=$4

# Check input argument
if [ -z "${capfile}" ] ; then
	echo "Usage:"
	echo "    $0 <CPU IP> <Cap File> [<user name>] [<password>]"
	echo ""
	echo "Note: if not enter, default username is root and default pasword is amp1234"
	exit
fi

# Check if the input file is *.cap
capname=$(echo "${capfile%*}" | grep -Eo ".cap$" | grep -Eo cap)
if [ "${capname}" != "cap" ]; then
	echo "Error: $capfile is not *.cap file"
	exit
fi

# Default username and password if not set
if [ -z "${usr}" ] ; then
	usr="root"
fi
if [ -z "${pass}" ] ; then
	pass="amp1234"
fi

# Execute fwupdate
fwupdate $cpuip $capfile $usr $pass

