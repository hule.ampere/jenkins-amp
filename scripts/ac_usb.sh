#!/bin/sh

CONFIG_FILE="$PWD/CONFIG"
COUNTER=5
if [ ! -f $CONFIG_FILE ]; then
	echo "NOT FOUND THE CONFIG FILE"
	exit -1
fi

while true; do
	if [ $COUNTER -eq 0 ]; then
		break
	fi
	if [ -z $BMC_IPADDRESS ]; then
		source $CONFIG_FILE
		sleep 5s
		COUNTER=$[$COUNTER-1]
		continue
	fi

	break
done

cpuip="10.38.65.23"
pass="root"
user="root"

#./amp-bmc-ast2500/tools/Yafuflash/Linux_x86_64/Yafuflash -nw -ip $BOARD_IP -u $IPMI_USER -p $IPMI_PASS -mse 3 -fb ./BUILDS/bmc/rom.ima
TEST_TIMES=10
AC_OFF_CONNECT="./bin/acpower_on_off 10.38.64.16 apc apc-c 1 ${POWER_OFF_CMD}"
AC_ON_CONNECT="./bin/acpower_on_off 10.38.64.16 apc apc-c 1 ${POWER_ON_CMD}"
POWER_ON_CMD="on"
POWER_OFF_CMD="off"
POWER_STATUS_CMD="power status"
PING="ping -c 2 $BMC_IPADDRESS"

if [ -z $BMC_IPADDRESS ]; then
	echo "BMC IP ADDRESS IS NOT FOUND"
	exit -1
fi

if [ -f test.done ]; then
	rm -rf test.done
fi

while [ $TEST_TIMES -gt 0 ]; do
	echo "TEST TIME(s): " $TEST_TIMES
	$AC_OFF_CONNECT
	sleep 5s
	$AC_ON_CONNECT
	sleep 210s
	$PING
	sleep 2s
  sshpass -p "${pass}" ssh -t ${user}@${cpuip} "/root/scripts/check_usb.sh"

	TEST_TIMES=$[$TEST_TIMES-1]
done

echo "DONE" > test.done

./scripts/killall.sh
