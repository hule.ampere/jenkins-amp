#!/usr/bin/env expect
global env

# -----------------------------------------------------------------------------
set timeout -1
set name [lindex $argv 0]
set port [lindex $argv 1]
set user [lindex $argv 2]
set password [lindex $argv 3]
set CONFIG_FILE "CONFIG"

# -----------------------------------------------------------------------------
# load commands from commands-file
#set f [open "$file_commands"]
#set commands [split [read $f] "\n"]
#close $f

# -----------------------------------------------------------------------------
# this spawns the telnet program and connects it to the variable name

spawn telnet $name $port 

set inet_expr {ip a | grep bond0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*'}

expect "Escape character is '^]'." {
	send  "\r"
}

expect {
	"login:" { 
		send "$user\r"
		expect "assword:"
		send "$password\r"
		expect " $ "
	}
	" $ " {
	}
}

send "$inet_expr \r"
after 1000

expect -re {([0-9]*\.){3}[0-9]*} {
	set ip ""
	regexp {([0-9]*\.){3}[0-9]*} $expect_out(buffer) ip

	exec sed -i "/BMC_IPADDRESS/d" $CONFIG_FILE

	set outfile [open $CONFIG_FILE a+]
	puts $outfile "BMC_IPADDRESS=\"$ip\""
	close $outfile
}

expect  " $ " {
	exp_continue
}

#puts $env(BMC_IP)

# -----------------------------------------------------------------------------
# iterate over the commands
#foreach cmd $commands {
#	send "$cmd\r"
#	expect "~ $"
#}

# -----------------------------------------------------------------------------
# close the connection
#send "exit\r"

# -----------------------------------------------------------------------------
# instead of closing the connection, it would also be possible to hand the
# control over to the user by calling the following command:

interact

