#!/bin/sh -x

CONFIG_FILE="$PWD/CONFIG"
CONSOLE_PID=$$
COUNTER=5

trap 'finish' SIGKILL SIGTERM SIGABRT SIGINT

finish() {
	echo "TEST DONE"
	exit 0
}

if [ ! -f $CONFIG_FILE ]; then
	echo "NOT FOUND THE CONFIG FILE"
	exit -1
fi

while true; do
	if [ $COUNTER -eq 0 ]; then
		break
	fi
	if [ -z $BMC_IPADDRESS ]; then
		source $CONFIG_FILE
		sleep 5s
		COUNTER=$[$COUNTER-1]
		continue
	fi

	break
done

if [ -z $BMC_IPADDRESS ]; then
	echo "BMC IP ADDRESS IS NOT FOUND"
	exit -1
fi

if [ ! -d "$PWD/$PID_DIR" ]; then
	mkdir -p "$PWD/$PID_DIR"
fi
echo $CONSOLE_PID > $BIOS_CONSOLE_PID_FILE

if [ -f $BIOS_LOCK_PID_FILE ]; then
	PID=$(cat $BIOS_LOCK_PID_FILE)
	if [ -n "$PID" -a -e /proc/$PID ]; then
		kill $PID
	fi
fi 

echo "===== TEST: $(date +%Y%m%d_%H%M%S) =====" >> $BIOS_CONSOLE_LOG_FILE
./scripts/sol.sh $BMC_IPADDRESS $BMC_USERNAME $BMC_PASSWORD > $BIOS_CONSOLE_LOG_FILE 2>&1 &
PID=$!
echo $PID > $BIOS_LOCK_PID_FILE

tail -f $BIOS_CONSOLE_LOG_FILE 
#& echo $! > $BIOS_TAIL_PID