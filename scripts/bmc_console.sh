#!/bin/sh

IP_ADDRESS=$1
PORT=$2
CONFIG_FILE="$PWD/CONFIG"
CONSOLE_PID=$$

trap 'finish' SIGKILL SIGTERM SIGABRT SIGINT

finish() {
	echo "TEST DONE"
	exit 0
}
# Check and include config file
if [ ! -f $CONFIG_FILE ]; then
	echo "NOT FOUND THE CONFIG FILE"
	exit -1
fi

source $CONFIG_FILE

if [ -z $BMC_USERNAME ] || [ -z $BMC_PASSWORD ]; then
	echo "NOT DEFINE BMC USERNAME OR PASSWORD"
	exit -1
fi

if [ ! -d "$PWD/$PID_DIR" ]; then
	mkdir -p "$PWD/$PID_DIR"
fi
# Save current PID 
echo $CONSOLE_PID > $BMC_CONSOLE_PID_FILE

# Check if exist telnet PID exist
if [ -f $BMC_LOCK_PID_FILE ]; then
	PID=$(cat $BMC_LOCK_PID_FILE)
	if [ -n "$PID" -a -e /proc/$PID ]; then
		kill $PID
	fi
fi

# Create new telnet process
echo "===== TEST: $(date +%Y%m%d_%H%M%S) =====" >> $BMC_CONSOLE_LOG_FILE
./scripts/telnet.sh $IP_ADDRESS $PORT $BMC_USERNAME $BMC_PASSWORD >> $BMC_CONSOLE_LOG_FILE 2>&1 &
PID=$!
echo $PID > $BMC_LOCK_PID_FILE

# Capture the log
tail -f $BMC_CONSOLE_LOG_FILE 
#& echo $! > $BMC_TAIL_PID
