#!/bin/sh

CONFIG_FILE="$PWD/CONFIG"
COUNTER=5
if [ ! -f $CONFIG_FILE ]; then
	echo "NOT FOUND THE CONFIG FILE"
	exit -1
fi

while true; do
	if [ $COUNTER -eq 0 ]; then
		break
	fi
	if [ -z $BMC_IPADDRESS ]; then
		source $CONFIG_FILE
		sleep 5s
		COUNTER=$[$COUNTER-1]
		continue
	fi

	break
done

TEST_TIMES=2
IPMI_CONNECT="ipmitool -I lanplus -H $BMC_IPADDRESS -U $BMC_USERNAME -P $BMC_PASSWORD"
POWER_ON_CMD="power on"
POWER_OFF_CMD="power aoff"
POWER_STATUS_CMD="power status"
PING="ping -c 20 $BMC_IPADDRESS"

if [ -z $BMC_IPADDRESS ]; then
	echo "BMC IP ADDRESS IS NOT FOUND"
	exit -1
fi

if [ -f test.done ]; then
	rm -rf test.done
fi

while [ $TEST_TIMES -gt 0 ]; do
	echo "TEST TIME(s): " $TEST_TIMES
	$IPMI_CONNECT $POWER_OFF_CMD
	sleep 10s
	$IPMI_CONNECT $POWER_STATUS_CMD
	sleep 2s
	$IPMI_CONNECT $POWER_ON_CMD
	sleep 200s
	$IPMI_CONNECT $POWER_STATUS_CMD
	sleep 2s
	$PING
	sleep 2s
	TEST_TIMES=$[$TEST_TIMES-1]
done

echo "DONE" > test.done

./scripts/killall.sh
