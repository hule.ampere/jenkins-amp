#!/bin/bash
#memtester is used for MFG memory test.
#Author by Lenovo WWTE Huifeng Li,2019/01/24
echo "memtester is used for MFG memory test,Author: WWTE Huifeng Li."
echo
cpu_count=`cat /proc/cpuinfo |grep processor|tail -n 1|awk '{print $3}'`
cpu_count_test=$((cpu_count+1)) 

free_mem=`free |grep Mem:|awk '{print $4}'`
used_mem=$((free_mem*90/102400/${cpu_count_test}))

memtester="./memtester"
if [ ! -f $memtester ];then
    echo "$memtester is not exist, please check again for /dfcxact/product/common/hytools/ARM_Ampere_Tool/!"
    exit 1
else
    cp $memtester /usr/bin
fi

echo "+--------------------------------+"
echo "|       Start memtester Test     |"
echo "+--------------------------------+"
echo "Test start time->"
date -d today +"%Y-%m-%d %T"

loop=1
echo "Run memtester for the totally ${cpu_count_test} processes at the same time"
while [ $loop -le ${cpu_count} ] ;do
  echo "Run the $loop into background to test"
  echo "memtester ${used_mem}M 1 >memtester_logfile_$loop.txt &"
  memtester ${used_mem}M 1 >/home/memtester_logfile_$loop.txt &
  let "loop+=1"
done
#The last loop donot run into background, it should run normally to let process know when the memtester finish.
echo "Run the ${cpu_count_test} into background to test"
echo "memtester ${used_mem}M 1 >memtester_logfile_32.txt "
memtester ${used_mem}M 1 |tee -i /home/memtester_logfile_$loop.txt
echo "Test end time->"
date -d today +"%Y-%m-%d %T"
echo "Sleep 120s"
sleep 120
echo "Check the test results"
cat /home/memtester_logfile*.txt |grep -ie "failure" -ie "error" -ie "fail"
if [ $? -eq 0 ] ;then
  cat /home/memtester_logfile*.txt |grep -ie "failure" -ie "error" -ie "fail"
  echo "ERROR:memtester test failed,please check the test results log"
  exit 1
else
  echo "memtester test PASS!"
fi

