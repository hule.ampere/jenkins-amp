# Jenkins Guide


1.  Install:
```
# Java
sudo apt update
sudo apt install openjdk-8-jdk
# Add the Jenkins Debian repository
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
# Jenkins
sudo apt update
sudo apt install jenkins
# Jenkins service
systemctl start jenkins
systemctl status jenkins
─:▶ systemctl status jenkins
● jenkins.service - LSB: Start Jenkins at boot time
   Loaded: loaded (/etc/init.d/jenkins; bad; vendor preset: enabled)
   Active: active (exited) since Mon 2019-06-03 10:00:27 +07; 3 days ago
     Docs: man:systemd-sysv-generator(8)
    Tasks: 0
   Memory: 0B
      CPU: 0
```

2.  Access Jenkins:
```
http://your_ip_or_domain:8080
```

